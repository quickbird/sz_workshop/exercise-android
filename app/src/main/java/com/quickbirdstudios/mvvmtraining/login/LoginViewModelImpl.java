package com.quickbirdstudios.mvvmtraining.login;

import android.arch.lifecycle.ViewModel;

import com.quickbirdstudios.mvvmtraining.util.ValidationUtils;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by Malte Bucksch on 10/04/2018.
 */
public class LoginViewModelImpl extends ViewModel implements LoginViewModel {
    private BehaviorSubject<String> emailText = BehaviorSubject.createDefault("");
    private BehaviorSubject<String> passwordText = BehaviorSubject.createDefault("");

    /**
     * TODO PART 2: VIEWMODEL IMPLEMENTATION
     * FOR PART 1:
     * @see LoginViewModel
     */
    // inputs
    // TODO 1 implement the input-getters by using the above mentioned BehaviorSubjects

    // outputs
    // TODO 2 implement the output-getters by applying operators on the input-getters
}