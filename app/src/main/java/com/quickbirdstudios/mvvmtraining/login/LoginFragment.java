package com.quickbirdstudios.mvvmtraining.login;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.quickbirdstudios.mvvmtraining.R;
import com.trello.rxlifecycle2.components.support.RxFragment;

/**
 * Created by Malte Bucksch on 05/06/2018.
 */
public class LoginFragment extends RxFragment {
    private LoginViewModel loginViewModel;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModelImpl.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EditText email = view.findViewById(R.id.email);
        EditText password = view.findViewById(R.id.password);
        Button loginButton = view.findViewById(R.id.login);

        /**
         * TODO PART 3: FRAGMENT IMPLEMENTATION
         * FOR PART 1:
         * @see LoginViewModel
         */
        // supply inputs to ViewModel
        // TODO 1: supply inputs to ViewModel for changes of the Email-EditText
        // TODO 2: supply inputs to ViewModel for changes of the Password-EditText

        // subscribe to outputs of ViewModel
        // TODO 3: show error at EditText if email is invalid (editText.setError("..")
        // TODO 4: show error at EditText if password is invalid (editText.setError("..")
        // TODO 5: disable the loginButton while email or password is not valid


        // TODO EXTRA (do this at the end if you still have time and want a challenge)
        // TODO EXTRA 1: the user needs to agree to the AGB to login
        // TODO EXTRA 1: add a CheckBox to the XML-layout and ONLY enable the login button if the
        // TODO EXTRA 1: user checked the CheckBox (agreed to the AGB)

        // TODO EXTRA (do this at the end if you still have time and want a challenge)
        // TODO EXTRA 2: show the Translator-Fragment when the user clicked the Login-Button
    }
}
