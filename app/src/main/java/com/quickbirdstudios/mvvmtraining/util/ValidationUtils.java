package com.quickbirdstudios.mvvmtraining.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class ValidationUtils {
    public static boolean isEmailValid(String email){
        if(email == null)return false;

        final String EMAIL_FORMAT = "\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,}\\b";

        Pattern pattern = Pattern.compile(EMAIL_FORMAT, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email.trim());

        return matcher.matches();
    }

    public static boolean isPasswordValid(String password){
        return password != null && password.length() > 4;
    }
}
